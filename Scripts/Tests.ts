﻿///<reference path="typings/jasmine/jasmine.d.ts"/>
///<reference path="typings/knockout/knockout.d.ts"/>

/// <chutzpah_reference path="knockout-3.1.0.js" />

class Customer {
     name: KnockoutObservable<string> = ko.observable("");
}

describe("Customer tests", () => {
    var customer = new Customer();

    it("Can set customer name", () => {
        customer.name("Sean");
        expect(customer.name()).toBe("Sean");
    });
});