﻿///<reference path="typings/jasmine/jasmine.d.ts"/>
///<reference path="typings/knockout/knockout.d.ts"/>
/// <chutzpah_reference path="knockout-3.1.0.js" />
var Customer = (function () {
    function Customer() {
        this.name = ko.observable("");
    }
    return Customer;
})();

describe("Customer tests", function () {
    var customer = new Customer();

    it("Can set customer name", function () {
        customer.name("Sean");
        expect(customer.name()).toBe("Sean");
    });
});
//# sourceMappingURL=Tests.js.map
